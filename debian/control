Source: gatb-core
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Nadiya Sitdykova <rovenskasa@gmail.com>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               d-shlibs,
               cmake,
               libcppunit-dev,
               libhdf5-dev,
               libboost-dev,
               libjsoncpp-dev,
               doxygen,
               graphviz
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/gatb-core
Vcs-Git: https://salsa.debian.org/med-team/gatb-core.git
Homepage: https://github.com/GATB/gatb-core
Rules-Requires-Root: no

Package: gatb-core
Architecture: any-amd64 arm64 loong64 mips64el ppc64el ia64 ppc64 riscv64 sparc64 alpha
Depends: ${shlibs:Depends},
         ${misc:Depends},
Description: Genome Analysis Toolbox with de-Bruijn graph
 The GATB-CORE project provides a set of highly efficient
 algorithms to analyse NGS data sets. These methods enable
 the analysis of data sets of any size on multi-core desktop
 computers, including very huge amount of reads data coming
 from any kind of organisms such as bacteria, plants,
 animals and even complex samples (e.g. metagenomes).
 Read more about GATB at https://gatb.inria.fr/.
 By itself GATB-CORE is not an NGS data analysis tool.
 However, it can be used to create such tools. There already
 exist a set of ready-to-use tools relying on GATB-CORE
 library: see https://gatb.inria.fr/software/

Package: libgatbcore3
Architecture: any-amd64 arm64 loong64 mips64el ppc64el ia64 ppc64 riscv64 sparc64 alpha
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: dynamic library of the Genome Analysis Toolbox
 The GATB-CORE project provides a set of highly efficient
 algorithms to analyse NGS data sets. These methods enable
 the analysis of data sets of any size on multi-core desktop
 computers, including very huge amount of reads data coming
 from any kind of organisms such as bacteria, plants,
 animals and even complex samples (e.g. metagenomes).
 Read more about GATB at https://gatb.inria.fr/.
 By itself GATB-CORE is not an NGS data analysis tool.
 However, it can be used to create such tools. There already
 exist a set of ready-to-use tools relying on GATB-CORE
 library: see https://gatb.inria.fr/software/
 .
 This package contains the dynamic library.

Package: libgatbcore-dev
Architecture: any-amd64 arm64 loong64 mips64el ppc64el ia64 ppc64 riscv64 sparc64 alpha
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends},
         libgatbcore3 (= ${binary:Version})
Description: development library of the Genome Analysis Toolbox
 The GATB-CORE project provides a set of highly efficient
 algorithms to analyse NGS data sets. These methods enable
 the analysis of data sets of any size on multi-core desktop
 computers, including very huge amount of reads data coming
 from any kind of organisms such as bacteria, plants,
 animals and even complex samples (e.g. metagenomes).
 Read more about GATB at https://gatb.inria.fr/.
 By itself GATB-CORE is not an NGS data analysis tool.
 However, it can be used to create such tools. There already
 exist a set of ready-to-use tools relying on GATB-CORE
 library: see https://gatb.inria.fr/software/
 .
 This package contains the static library and the header files
 of the gatb-core library.

Package: gatb-core-testdata
Architecture: all
Depends: ${misc:Depends},
         gatb-core
Description: Genome Analysis Toolbox with de-Bruijn graph (test data)
 The GATB-CORE project provides a set of highly efficient
 algorithms to analyse NGS data sets. These methods enable
 the analysis of data sets of any size on multi-core desktop
 computers, including very huge amount of reads data coming
 from any kind of organisms such as bacteria, plants,
 animals and even complex samples (e.g. metagenomes).
 Read more about GATB at https://gatb.inria.fr/.
 By itself GATB-CORE is not an NGS data analysis tool.
 However, it can be used to create such tools. There already
 exist a set of ready-to-use tools relying on GATB-CORE
 library: see https://gatb.inria.fr/software/
 .
 This package contains some data to test the library.
