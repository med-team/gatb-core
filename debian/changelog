gatb-core (1.4.2+dfsg-13) unstable; urgency=medium

  * Add build support for loongarch64
    Closes: #1058714
  * Cleanup symbols file manually

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jan 2024 20:48:47 +0100

gatb-core (1.4.2+dfsg-12) unstable; urgency=medium

  * Team upload.
  * d/*.symbols*: update table.
    Marking several symbols leaked by the compiler as optional fixes build
    failures with gcc-13 and enables link time optimizations.
    (Closes: #1042146, #1015412)
  * d/*.symbols*: Build-Depends-Package: libgatbcore-dev.
  * d/control: declare compliance to standards version 4.6.2.

 -- Étienne Mollier <emollier@debian.org>  Thu, 17 Aug 2023 17:01:45 +0200

gatb-core (1.4.2+dfsg-11) unstable; urgency=medium

  * Architecture: any-amd64 arm64 mips64el ppc64el s390x ia64 ppc64 riscv64
                  sparc64 alpha
    Closes: #1023228
  * Fix watch file

 -- Andreas Tille <tille@debian.org>  Fri, 25 Nov 2022 20:46:35 +0100

gatb-core (1.4.2+dfsg-10) unstable; urgency=medium

  * Team upload.
  * Add autopkgtests

 -- Mohammed Bilal <mdbilal@disroot.org>  Sat, 03 Sep 2022 14:05:14 +0000

gatb-core (1.4.2+dfsg-9) unstable; urgency=medium

  * Adapt symbols to gcc-12
    Closes: #1012934
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 24 Jul 2022 17:06:13 +0200

gatb-core (1.4.2+dfsg-8) unstable; urgency=medium

  * Fix symbols file
    Closes: #1002217

 -- Andreas Tille <tille@debian.org>  Wed, 22 Dec 2021 09:34:27 +0100

gatb-core (1.4.2+dfsg-7) unstable; urgency=medium

  [ Steffen Moeller ]
  * Applied patch from July 2020 (past 1.4.2 release) to allow compilation
    of minia - just exposing a few variables in the template class.
    Request to tag a new release on https://github.com/GATB/gatb-core/issues/38

  [ Andreas Tille ]
  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * Fix ftbfs with gcc-11 by adapting symbols file
    Closes: #984145

 -- Andreas Tille <tille@debian.org>  Mon, 18 Oct 2021 13:51:05 +0200

gatb-core (1.4.2+dfsg-6) unstable; urgency=medium

  * Use int as return of fgetc
    Closes: #954273
  * Standards-Version: 4.5.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 02 Dec 2020 17:09:31 +0100

gatb-core (1.4.2+dfsg-5) unstable; urgency=medium

  * Fix installation of test data to really build on Architecture all
  * Fix installation of unit test script gatb-core-cppunit

 -- Andreas Tille <tille@debian.org>  Fri, 24 Jul 2020 10:01:31 +0200

gatb-core (1.4.2+dfsg-4) unstable; urgency=medium

  * Enable Architecture all only builds

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jul 2020 22:21:16 +0200

gatb-core (1.4.2+dfsg-3) unstable; urgency=medium

  * debhelper-compat 13 (routine-update)
  * Update symbols for gcc-10
    Closes: #957254

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jul 2020 13:41:55 +0200

gatb-core (1.4.2+dfsg-2) unstable; urgency=medium

  * Team Upload.
  * Update symbols file (Closes: #960414)

 -- Nilesh Patra <npatra974@gmail.com>  Wed, 10 Jun 2020 21:01:03 +0530

gatb-core (1.4.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Return to regular releases instead of git snaphots
  * add multi-arch hints
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Submit.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 13 Apr 2020 13:02:28 +0200

gatb-core (1.4.1+git20191209.9398f28+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add -latomic where needed to fix the build on armel, mipsel & powerpc.
  * new patch to avoid "-msse2" on build hosts that use qemu
  * Simplify library dependencies. Closes: #947964.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 05 Jan 2020 22:40:32 +0100

gatb-core (1.4.1+git20191209.9398f28+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Bug-Database, Repository.

 -- Andreas Tille <tille@debian.org>  Fri, 27 Dec 2019 22:03:59 +0100

gatb-core (1.4.1+git20191130.664696c+dfsg-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * New upstream version
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Repository-Browse.
  * Remove obsolete fields Name from debian/upstream/metadata.

  [ Andreas Tille ]
  * Bump Debian own invented SOVERSION due to massive changes in upstream
    ABI

 -- Andreas Tille <tille@debian.org>  Thu, 05 Dec 2019 12:34:00 +0100

gatb-core (1.4.1+git20190813.a73b6dd+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Fix symbols file to match output from gcc9
    Closes: #925690
  * List missing dependencies explicitly
    Closes: #931403

 -- Andreas Tille <tille@debian.org>  Tue, 20 Aug 2019 12:51:53 +0200

gatb-core (1.4.1+git20181225.44d5a44+dfsg-3) unstable; urgency=medium

  * Fix symbols
    Closes: #930368

 -- Andreas Tille <tille@debian.org>  Wed, 19 Jun 2019 12:25:30 +0200

gatb-core (1.4.1+git20181225.44d5a44+dfsg-2) unstable; urgency=medium

  * Rebuild for new version of gcc to fix symbols
    Closes: #924849

 -- Andreas Tille <tille@debian.org>  Mon, 18 Mar 2019 09:45:17 +0100

gatb-core (1.4.1+git20181225.44d5a44+dfsg-1) unstable; urgency=medium

  * New upstream checkout
  * Adapt symbols file to new upstream code
  * Bump SOVERSION due to ABI changes

 -- Andreas Tille <tille@debian.org>  Thu, 24 Jan 2019 11:50:33 +0100

gatb-core (1.4.1+git20180206.6f8fce8+dfsg-2) unstable; urgency=medium

  * Provide symbols for amd64 only
    Closes: #920330

 -- Andreas Tille <tille@debian.org>  Thu, 24 Jan 2019 10:28:21 +0100

gatb-core (1.4.1+git20180206.6f8fce8+dfsg-1) unstable; urgency=medium

  * New upstream commit to fit needs of minia and discosnp
  * Add symbols file
  * Bump SOVERSION due to changed symbols

 -- Andreas Tille <tille@debian.org>  Tue, 22 Jan 2019 16:58:53 +0100

gatb-core (1.4.1+dfsg-4) unstable; urgency=medium

  * Install json.hpp since it is referenced in header files
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Tue, 22 Jan 2019 14:50:38 +0100

gatb-core (1.4.1+dfsg-3) unstable; urgency=medium

  * Make sure target dir exists also for Architecture: all build
    Closes: #903613
  * Standards-Version: 4.2.1
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/control

 -- Andreas Tille <tille@debian.org>  Sat, 15 Dec 2018 10:22:40 +0100

gatb-core (1.4.1+dfsg-2) unstable; urgency=medium

  * Install cmake files
  * Fix binary only build
    Closes: #902161

 -- Andreas Tille <tille@debian.org>  Fri, 06 Jul 2018 12:01:54 +0200

gatb-core (1.4.1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #873044)

 -- Andreas Tille <tille@debian.org>  Mon, 14 May 2018 21:57:34 +0200
